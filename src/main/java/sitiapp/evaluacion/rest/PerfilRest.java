package sitiapp.evaluacion.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sitiapp.evaluacion.model.Respuesta;
import sitiapp.evaluacion.service.PerfilService;

@RestController
@RequestMapping("/perfiles")
public class PerfilRest {

    @Autowired
    private PerfilService perfilService;

    @GetMapping
    private ResponseEntity<Respuesta> getPerfiles(){
        Respuesta respuesta = new Respuesta();
        try{
            respuesta.exito = 1;
            respuesta.data = perfilService.findAll();
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }
}
