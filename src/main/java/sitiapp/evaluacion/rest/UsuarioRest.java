package sitiapp.evaluacion.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sitiapp.evaluacion.model.Perfil;
import sitiapp.evaluacion.model.Respuesta;
import sitiapp.evaluacion.model.Usuario;
import sitiapp.evaluacion.security.dto.NuevoUsuario;
import sitiapp.evaluacion.service.UsuarioService;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/usuarios")
public class UsuarioRest {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @GetMapping
    private ResponseEntity<Respuesta> getUsuarios(){
        Respuesta respuesta = new Respuesta();
        try{
            respuesta.exito = 1;
            respuesta.data = usuarioService.findAll();
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @GetMapping("{id}")
    private ResponseEntity<Respuesta> getUsuarioById(@PathVariable("id") Integer id_usuario){
        Respuesta respuesta = new Respuesta();
        try{
            respuesta.exito = 1;
            respuesta.data = usuarioService.findById(id_usuario);
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @PostMapping("/nuevo")
    public ResponseEntity<Respuesta> nuevo(@Valid @RequestBody NuevoUsuario nuevoUsuario, BindingResult bindingResult){
        Respuesta respuesta = new Respuesta();
        try{
            if(bindingResult.hasErrors()){
                respuesta.mensaje = "campos mal puestos";
                respuesta.exito = 0;
                return ResponseEntity.ok(respuesta);
            }
            if(usuarioService.existsByUsuario(nuevoUsuario.getUsuario())){
                respuesta.mensaje = "ese usuario ya existe";
                respuesta.exito = -1;
                return ResponseEntity.ok(respuesta);
            }
            Perfil perfil = new Perfil();
            perfil.setId(nuevoUsuario.getPerfil().getId());
            perfil.setNombre(nuevoUsuario.getPerfil().getNombre());
            Usuario usuario = new Usuario(0,nuevoUsuario.getNombre(),nuevoUsuario.getApellido(),nuevoUsuario.getUsuario(),
                    passwordEncoder.encode(nuevoUsuario.getPassword()),perfil,1);
            usuario.setPerfil(perfil);
            usuario.setId(0);
            usuarioService.save(usuario);
            respuesta.exito = 1;
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    private ResponseEntity<Respuesta> updateUsuario(@RequestBody Usuario upUsuario){
        Respuesta respuesta = new Respuesta();
        try{
            Optional<Usuario>  actual = usuarioService.findById(upUsuario.getId());
            Integer exitoUpdate = usuarioService.updateUsuario(actual.get().getId(),upUsuario.getNombre(),upUsuario.getApellido(),upUsuario.getUsuario(),actual.get().getContraseña(),upUsuario.getPerfil());
            if (exitoUpdate != 0){
                respuesta.exito = 1;
            }
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @DeleteMapping("{id}")
    private ResponseEntity<Respuesta> deleteUsuario(@PathVariable("id") Integer id_usuario){
        Respuesta respuesta = new Respuesta();
        try{
            Integer exitoDelete = usuarioService.deleteUsuario(id_usuario);
            if(exitoDelete != 0){
                respuesta.exito = 1;
            }
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }
}
