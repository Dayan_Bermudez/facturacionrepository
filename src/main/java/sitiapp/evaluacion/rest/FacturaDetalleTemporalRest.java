package sitiapp.evaluacion.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sitiapp.evaluacion.model.Factura;
import sitiapp.evaluacion.model.FacturaDetalleTemporal;
import sitiapp.evaluacion.model.Facturacion;
import sitiapp.evaluacion.model.Respuesta;
import sitiapp.evaluacion.service.FacturaDetalleTemporalService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/facturaDetalleTemporal")
public class FacturaDetalleTemporalRest {

    @Autowired
    private FacturaDetalleTemporalService facturaDetalleTemporalService;

    @GetMapping
    public ResponseEntity<Respuesta> getFacturaDetallesTemporal(){
        Respuesta respuesta = new Respuesta();
        try{
            respuesta.data = facturaDetalleTemporalService.findAll();
            respuesta.exito = 1;
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Respuesta> createFacturaDetalleTemporal(@RequestBody FacturaDetalleTemporal facturaDetallesTemp ){
        Respuesta respuesta = new Respuesta();
        try{
            facturaDetalleTemporalService.save(facturaDetallesTemp);
            respuesta.exito = 1;
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @DeleteMapping
    public ResponseEntity<Respuesta> deleteFacturaDetalleTemporal(){
        Respuesta respuesta = new Respuesta();
        try{
            facturaDetalleTemporalService.deleteAll();
            respuesta.exito = 1;
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Respuesta> deleteFacturaDetalleTemporal(@PathVariable("id") Integer id_facturaDetalle){
        Respuesta respuesta = new Respuesta();
        try{
            facturaDetalleTemporalService.deleteById(id_facturaDetalle);
            respuesta.exito = 1;
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }
}
