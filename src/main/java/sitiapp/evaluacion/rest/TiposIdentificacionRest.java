package sitiapp.evaluacion.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sitiapp.evaluacion.model.Respuesta;
import sitiapp.evaluacion.service.TiposIdentificacionService;

@RestController
@RequestMapping("tipos/identificacion")
public class TiposIdentificacionRest {

    @Autowired
    private TiposIdentificacionService tiposIdentificacionService;

    @GetMapping
    private ResponseEntity<Respuesta> getTiposIdentificacion(){
        Respuesta respuesta = new Respuesta();
        try{
            respuesta.exito = 1;
            respuesta.data = tiposIdentificacionService.findAllOrderById();
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }
}
