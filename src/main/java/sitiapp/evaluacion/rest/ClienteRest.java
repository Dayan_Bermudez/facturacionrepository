package sitiapp.evaluacion.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import sitiapp.evaluacion.model.Cliente;
import sitiapp.evaluacion.model.Respuesta;
import sitiapp.evaluacion.service.ClienteService;

@RestController
@RequestMapping("/clientes")
public class ClienteRest {

    @Autowired
    private ClienteService clienteService;

    @GetMapping
    private ResponseEntity<Respuesta> getClientes(){
        Respuesta respuesta = new Respuesta();
        try{
            respuesta.exito = 1;
            respuesta.data = clienteService.findAll();
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @GetMapping("{id}")
    private ResponseEntity<Respuesta> getClienteById(@PathVariable("id") Integer id_cliente){
        Respuesta respuesta = new Respuesta();
        try{
            respuesta.exito = 1;
            respuesta.data = clienteService.findById(id_cliente);
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    private ResponseEntity<Respuesta> createCliente(@RequestBody Cliente newCliente){
        Respuesta respuesta = new Respuesta();
        try{
            if(clienteService.existsByIdentificacion(newCliente.getIdentificacion())){
                respuesta.exito = -1;
                return ResponseEntity.ok(respuesta);
            }
            respuesta.exito = 1;
            newCliente.setActivo(1);
            clienteService.save(newCliente);
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    private ResponseEntity<Respuesta> updateCliente(@RequestBody Cliente upCliente){
        Respuesta respuesta = new Respuesta();
        try{
            Integer exitoUpdate = clienteService.updateCliente(upCliente.getId(),upCliente.getIdentificacion(),upCliente.getRazonSocial(),upCliente.getFechaRegistro(),upCliente.getEstado(),upCliente.getIdTipoIdentificacion());
            if (exitoUpdate != 0){
                respuesta.exito = 1;
            }
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @DeleteMapping("{id}")
    private ResponseEntity<Respuesta> deleteCliente(@PathVariable("id") Integer id_cliente){
        Respuesta respuesta = new Respuesta();
        try{
            Integer exitoDelete = clienteService.deleteCliente(id_cliente);
            if(exitoDelete != 0){
                respuesta.exito = 1;
            }
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @GetMapping("/tipoId")
    @ResponseBody
    private ResponseEntity<Respuesta> getClienteByTipoIdentificacion(Integer id_tipo_buscado,Integer identificacion){
        Respuesta respuesta = new Respuesta();
        try{
            if(identificacion==0 && id_tipo_buscado!=0){
                respuesta.data = clienteService.getClientesByTipoIdetificacion(id_tipo_buscado);
            }

            if(identificacion!=0 && id_tipo_buscado==0){
                respuesta.data = clienteService.getClientesByIdetificacion(identificacion.toString());
            }

            if(identificacion!=0 && id_tipo_buscado!=0){
                respuesta.data = clienteService.getClientesByTipo_E_Idetificacion(id_tipo_buscado,identificacion.toString());
            }
            respuesta.exito = 1;
        }catch (Exception exception){
            respuesta.mensaje = exception.getMessage();
            respuesta.exito = 0;
        }
        return ResponseEntity.ok(respuesta);
    }
}
