package sitiapp.evaluacion.rest;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import sitiapp.evaluacion.model.*;
import sitiapp.evaluacion.service.FacturaDetalleService;
import sitiapp.evaluacion.service.FacturaDetalleTemporalService;
import sitiapp.evaluacion.service.FacturaService;

import javax.swing.text.Document;
import java.io.File;
import java.time.LocalDate;
import java.util.*;

@RestController
@RequestMapping("/facturacion")
public class FacturacionRest {

    @Autowired
    private FacturaService facturaService;
    @Autowired
    private FacturaDetalleService facturaDetalleService;
    @Autowired
    private FacturaDetalleTemporalService facturaDetalleTemporalService;


    @GetMapping("/facturas")
    public ResponseEntity<Respuesta> getFacturas(){
        Respuesta respuesta = new Respuesta();
        try{
            List<Facturacion> facturacion = new ArrayList<Facturacion>();
            List<Factura> facturas = facturaService.findAll();
            Facturacion facturacion1 = new Facturacion();
            for(Factura item: facturas){
                facturacion1.setFactura(item);
                facturacion1.setFacturaDetalle(facturaDetalleService.findAllByFacturaId(item.getId()));
                facturacion.add(facturacion1);
            }
            respuesta.exito = 1;
            respuesta.data = facturacion1;
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @GetMapping("/factura/{id}")
    public ResponseEntity<Respuesta> getFacturaId(@PathVariable("id") Integer id_factura){
        Respuesta respuesta = new Respuesta();
        try{
            String encoded = this.generarReporte(id_factura);
            respuesta.exito = 1;
            respuesta.data = encoded;
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Respuesta> createFacturacion(@RequestBody Facturacion newfacturacion){
        Respuesta respuesta = new Respuesta();
        try{
            Factura newFactura = new Factura();
            newFactura.setCliente(newfacturacion.getFactura().getCliente());
            newFactura.setId(facturaService.getTopBy()+1);
            newFactura.setFecha(LocalDate.now());
            newFactura.setActivo(1);

            List<FacturaDetalleTemporal> facturaDetallesTem = newfacturacion.getFacturaDetalleTemporal();
            List<FacturaDetalle> ListfacturaDetalles = new ArrayList<FacturaDetalle>();
            FacturaDetalle facturaDetalle;
            Integer total = 0;
            for (FacturaDetalleTemporal item: facturaDetallesTem){
                facturaDetalle = new FacturaDetalle();
                facturaDetalle.setFactura(newFactura);
                facturaDetalle.setProducto(item.getProducto());
                facturaDetalle.setValorUnitario(item.getProducto().getValorUnitario());
                facturaDetalle.setCantidad(item.getCantidad());
                facturaDetalle.setActivo(1);
                facturaDetalle.setTotal(item.getCantidad()* item.getValorUnitario());
                ListfacturaDetalles.add(facturaDetalle);
                facturaDetalle = null;
                total += item.getCantidad()* item.getValorUnitario();
            }

            newFactura.setTotalFactura(total);
            facturaService.save(newFactura);
            facturaDetalleService.saveAll(ListfacturaDetalles);
            respuesta.exito = 1;
            respuesta.data = this.generarReporte(newFactura.getId());
            facturaDetalleTemporalService.deleteAll();
       }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
       }
        return ResponseEntity.ok(respuesta);
    }

    private String generarReporte(Integer id_factura){
        String pdf = "";
        try{
            Factura factura = facturaService.findById(id_factura).get();
            List<FacturaDetalle> facturaDetalles = facturaDetalleService.findAllByFacturaId(id_factura);
            List<FacturacionGetBase> listFacturacion = new ArrayList<FacturacionGetBase>();
            FacturacionGetBase newFacturacionGetBase;
            for (FacturaDetalle factDet: facturaDetalles) {
                newFacturacionGetBase = new FacturacionGetBase();
                newFacturacionGetBase.setId_factura(factura.getId());
                newFacturacionGetBase.setFecha(factura.getFecha());
                newFacturacionGetBase.setTotal_factura(factura.getTotalFactura());
                newFacturacionGetBase.setId_producto(factDet.getProducto().getId());
                newFacturacionGetBase.setNombre(factDet.getProducto().getNombre());
                newFacturacionGetBase.setCantidad(factDet.getCantidad());
                newFacturacionGetBase.setValor_unitario(factDet.getProducto().getValorUnitario());
                newFacturacionGetBase.setTotal(factDet.getTotal());
                newFacturacionGetBase.setAbreviatura(factura.getCliente().getIdTipoIdentificacion().getAbreviatura());
                newFacturacionGetBase.setIdentificacion(factura.getCliente().getIdentificacion());
                newFacturacionGetBase.setRazon_social(factura.getCliente().getRazonSocial());
                listFacturacion.add(newFacturacionGetBase);
                newFacturacionGetBase = null;
            }

            File file = ResourceUtils.getFile("classpath:factura.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(listFacturacion);
            Map<String,Object> parameters = new HashMap<>();
            parameters.put("CreatedBy","Dayan");
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parameters,dataSource);

            //JasperExportManager.exportReportToPdfFile(jasperPrint,"C:\\Users\\dayan\\Desktop\\facturacion.pdf");
            byte[] filePDF = JasperExportManager.exportReportToPdf(jasperPrint);
            pdf =  Base64.getEncoder().encodeToString(filePDF);
        }catch (Exception exception){
            System.out.println(exception.getMessage());
        }
        return pdf;
    }
}
