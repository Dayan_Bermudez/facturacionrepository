package sitiapp.evaluacion.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import sitiapp.evaluacion.model.Producto;
import sitiapp.evaluacion.model.Respuesta;
import sitiapp.evaluacion.service.ProductoService;

import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/productos")
public class ProductoRest {

    @Autowired
    private ProductoService productoService;

    @GetMapping
    private ResponseEntity<Respuesta> getProductos(){
        Respuesta respuesta = new Respuesta();
        try{
            respuesta.exito = 1;
            respuesta.data = productoService.findAll();
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @GetMapping("{id}")
    private ResponseEntity<Respuesta> getProductoById(@PathVariable("id") Integer id_producto){
        Respuesta respuesta = new Respuesta();
        try{
            if(productoService.existsById(id_producto)){
                List<Producto> list = new ArrayList<Producto>();
                list.add(productoService.findById(id_producto).get());
                respuesta.data = list;
                respuesta.exito = 1;
            }else {
                respuesta.exito = 0;
            }
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    private ResponseEntity<Respuesta> createProducto(@RequestBody Producto newProducto){
        Respuesta respuesta = new Respuesta();
        try{
            if(productoService.existsByNombre(newProducto.getNombre())){
                respuesta.exito = -1;
                return ResponseEntity.ok(respuesta);
            }
            respuesta.exito = 1;
            newProducto.setActivo(1);
            productoService.save(newProducto);
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @PutMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    private ResponseEntity<Respuesta> updateCliente(@RequestBody Producto upProducto){
        Respuesta respuesta = new Respuesta();
        try{
            Integer exitoUpdate = productoService.updateProducto(upProducto.getId(),upProducto.getNombre(),upProducto.getEstado(),upProducto.getValorUnitario());
            if (exitoUpdate != 0){
                respuesta.exito = 1;
            }
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }

    @DeleteMapping("{id}")
    private ResponseEntity<Respuesta> deleteCliente(@PathVariable("id") Integer id_producto){
        Respuesta respuesta = new Respuesta();
        try{
            Integer exitoDelete = productoService.deleteProducto(id_producto);
            if(exitoDelete != 0){
                respuesta.exito = 1;
            }
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }
}
