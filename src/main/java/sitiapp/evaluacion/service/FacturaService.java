package sitiapp.evaluacion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.evaluacion.model.Factura;
import sitiapp.evaluacion.repository.FacturaRepository;
import java.util.List;
import java.util.Optional;

@Service
public class FacturaService {

    @Autowired
    private FacturaRepository facturaRepository;

    public List<Factura> findAll(){
        return facturaRepository.findAll();
    }

    public Optional<Factura> findById(Integer id_factura){
        return facturaRepository.findById(id_factura);
    }

    public <S extends Factura> S save(S entity) {
        return facturaRepository.save(entity);
    }

    public Integer deleteFactura(Integer id_factura){
        return facturaRepository.deleteFactura(id_factura);
    }

    public Integer getTopBy(){
        return facturaRepository.getTopBy();
    }
}
