package sitiapp.evaluacion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.evaluacion.model.FacturaDetalle;
import sitiapp.evaluacion.repository.FacturaDetalleRepository;

import java.util.List;

@Service
public class FacturaDetalleService {

    @Autowired
    private FacturaDetalleRepository facturaDetalleRepository;

    public List<FacturaDetalle> findAll(){
        return facturaDetalleRepository.findAll();
    }

    public List<FacturaDetalle> findAllByFacturaId(Integer id_factura){
        return facturaDetalleRepository.findAllByFacturaId(id_factura);
    }

    public Integer deleteFacturaDetalleByFacturaId(Integer id_factura){
        return facturaDetalleRepository.deleteFacturaDetalleByFacturaId(id_factura);
    }

    public <S extends FacturaDetalle> List<S> saveAll(Iterable<S> entities) {
        return facturaDetalleRepository.saveAll(entities);
    }
}
