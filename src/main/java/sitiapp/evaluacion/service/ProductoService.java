package sitiapp.evaluacion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.evaluacion.model.Producto;
import sitiapp.evaluacion.repository.ProductoRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {

    @Autowired
    private ProductoRepository productoRepository;

    public List<Producto> findAll() {
        return productoRepository.findAll();
    }

    public <S extends Producto> S save(S entity) {
        return productoRepository.save(entity);
    }

    public Optional<Producto> findById(Integer integer) {
        return productoRepository.findById(integer);
    }

    public Integer updateProducto(Integer id, String nombre, String estado, Integer valorUnitario) {
        return productoRepository.updateProducto(id,nombre,estado,valorUnitario);
    }

    public Integer deleteProducto(Integer id_producto) {
        return productoRepository.deleteProducto(id_producto);
    }

    public boolean existsById(Integer integer){
        return productoRepository.existsById(integer);
    }

    public boolean existsByNombre(String nombre){
        return productoRepository.existsByNombre(nombre);
    }
}
