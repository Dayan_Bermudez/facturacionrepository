package sitiapp.evaluacion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.evaluacion.model.TiposIdentificacion;
import sitiapp.evaluacion.repository.TiposIdentifiacionRepository;

import java.util.List;

@Service
public class TiposIdentificacionService {

    @Autowired
    private TiposIdentifiacionRepository tiposIdentifiacionRepository;

    public List<TiposIdentificacion> findAllOrderById() {
        return tiposIdentifiacionRepository.findAllOrderById();
    }
}
