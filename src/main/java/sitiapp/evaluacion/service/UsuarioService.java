package sitiapp.evaluacion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.evaluacion.model.Perfil;
import sitiapp.evaluacion.model.Usuario;
import sitiapp.evaluacion.repository.UsuarioRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public List<Usuario> findAll() {
        return usuarioRepository.findAll();
    }

    public <S extends Usuario> S save(S entity) {
        return usuarioRepository.save(entity);
    }

    public Optional<Usuario> findById(Integer id_usuario) {
        return usuarioRepository.findById(id_usuario);
    }

    public Integer updateUsuario(Integer id, String nombre, String apellido, String usuario, String contraseña, Perfil perfil) {
        return usuarioRepository.updateUsuario(id,nombre,apellido,usuario,contraseña,perfil);
    }

    public Integer deleteUsuario(Integer id_usuario) {
        return usuarioRepository.deleteUsuario(id_usuario);
    }

    public boolean existsByUsuario(String usuario){
        return usuarioRepository.existsByUsuario(usuario);
    }
}
