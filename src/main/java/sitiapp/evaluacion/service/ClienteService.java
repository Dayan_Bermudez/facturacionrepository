package sitiapp.evaluacion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.evaluacion.model.Cliente;
import sitiapp.evaluacion.model.TiposIdentificacion;
import sitiapp.evaluacion.repository.ClienteRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public List<Cliente> findAll() {
        return clienteRepository.findAll();
    }

    public Integer updateCliente(int id, String identificacion, String razon_social, LocalDate fecha_registro, String estado, TiposIdentificacion tipo_identificacion) {
        return clienteRepository.updateCliente(id,identificacion,razon_social,fecha_registro,estado,tipo_identificacion);
    }

    public Integer deleteCliente(Integer id_cliente) {
        return clienteRepository.deleteCliente(id_cliente);
    }

    public <S extends Cliente> S save(S entity) {
        return clienteRepository.save(entity);
    }

    public Optional<Cliente> findById(Integer integer) {
        return clienteRepository.findById(integer);
    }

    public List<Cliente> getClientesByTipo_E_Idetificacion(Integer id_tipo_buscado,String identificacion){
        return clienteRepository.getClientesByTipo_E_Idetificacion(id_tipo_buscado,identificacion);
    }

    public List<Cliente> getClientesByTipoIdetificacion(Integer id_tipo_buscado){
        return clienteRepository.getClientesByTipoIdetificacion(id_tipo_buscado);
    }

    public List<Cliente> getClientesByIdetificacion(String identificacion){
        return clienteRepository.getClientesByIdetificacion(identificacion);
    }

    public boolean existsByIdentificacion(String identificacion){
        return clienteRepository.existsByIdentificacion(identificacion);
    }
}
