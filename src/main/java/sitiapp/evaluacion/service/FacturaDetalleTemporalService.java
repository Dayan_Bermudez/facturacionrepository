package sitiapp.evaluacion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sitiapp.evaluacion.model.FacturaDetalleTemporal;
import sitiapp.evaluacion.repository.FacturaDetalleTemporalRepository;

import java.util.List;

@Service
public class FacturaDetalleTemporalService{

    @Autowired
    private FacturaDetalleTemporalRepository facturaDetalleTemporalRepository;

    public List<FacturaDetalleTemporal> findAll() {
        return facturaDetalleTemporalRepository.findAll();
    }

    public void deleteById(Integer integer) {
        facturaDetalleTemporalRepository.deleteById(integer);
    }

    public <S extends FacturaDetalleTemporal> S save(S entity) {
        return facturaDetalleTemporalRepository.save(entity);
    }

    public <S extends FacturaDetalleTemporal> List<S> saveAll(Iterable<S> entities) {
        return facturaDetalleTemporalRepository.saveAll(entities);
    }

    public void deleteAll() {
        facturaDetalleTemporalRepository.deleteAll();
    }
}
