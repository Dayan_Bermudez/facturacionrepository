package sitiapp.evaluacion.security.entity;

import lombok.Data;
import sitiapp.evaluacion.model.Perfil;

import java.util.Set;

@Data
public class UsuarioSecurity{
    private Integer id;
    private String nombre;
    private String apellido;
    private String usuario;
    private String contraseña;
    private Set<Perfil> perfiles;
}
