package sitiapp.evaluacion.security.dto;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Data
public class JwtDto {
    private String token;
    private String bearer = "Bearer";
    private String nombreUsuario;
    private String authoritie;

    public JwtDto() {
    }

    public JwtDto(String token, String nombreUsuario, String authority) {
        this.token = token;
        this.nombreUsuario = nombreUsuario;
        this.authoritie = authority;
    }
}
