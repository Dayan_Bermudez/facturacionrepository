package sitiapp.evaluacion.security.dto;

import com.sun.istack.NotNull;
import lombok.Data;
import sitiapp.evaluacion.model.Perfil;
import javax.validation.constraints.NotBlank;

@Data
public class NuevoUsuario {
    @NotBlank
    private String nombre;
    private String apellido;
    @NotBlank
    private String usuario;
    @NotBlank
    private String password;
    private Perfil perfil;

    public NuevoUsuario() {
    }
}
