package sitiapp.evaluacion.security.dto;

import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class LoginUsuario {
    @NotNull
    private String usuario;
    @NotNull
    private String password;

    public LoginUsuario() {
    }
}
