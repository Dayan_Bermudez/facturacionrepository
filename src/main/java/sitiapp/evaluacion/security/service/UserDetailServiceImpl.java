package sitiapp.evaluacion.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import sitiapp.evaluacion.model.Perfil;
import sitiapp.evaluacion.model.Usuario;
import sitiapp.evaluacion.security.entity.UsuarioPrincipal;
import sitiapp.evaluacion.security.entity.UsuarioSecurity;


import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String user) throws UsernameNotFoundException {
        Usuario usuario = userService.findByUsuario(user).get();
        UsuarioSecurity usuarioSecurity = new UsuarioSecurity();
        usuarioSecurity.setId(usuario.getId());
        usuarioSecurity.setNombre(usuario.getNombre());
        usuarioSecurity.setApellido(usuario.getApellido());
        usuarioSecurity.setUsuario(usuario.getUsuario());
        usuarioSecurity.setContraseña(usuario.getContraseña());
        Set<Perfil> perfils = new HashSet<>();
        perfils.add(usuario.getPerfil());
        usuarioSecurity.setPerfiles(perfils);
        return UsuarioPrincipal.build(usuarioSecurity);
    }
}
