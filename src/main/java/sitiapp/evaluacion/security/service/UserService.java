package sitiapp.evaluacion.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sitiapp.evaluacion.model.Usuario;
import sitiapp.evaluacion.repository.UsuarioRepository;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Optional<Usuario> findByUsuario(String usuario) {
        return usuarioRepository.findByUsuario(usuario);
    }

    public boolean existsByUsuario(String usuario) {
        return usuarioRepository.existsByUsuario(usuario);
    }

    public <S extends Usuario> S save(S entity){
        return usuarioRepository.save(entity);
    }
}
