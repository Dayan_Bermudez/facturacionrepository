package sitiapp.evaluacion.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sitiapp.evaluacion.model.Perfil;
import sitiapp.evaluacion.repository.PerfilRepository;

import java.util.Optional;

@Service
@Transactional
public class PerfilSecurityService {

    @Autowired
    private PerfilRepository perfilRepository;

    public Optional<Perfil> findByNombre(String nombre){
        return perfilRepository.findByNombre(nombre);
    }
}
