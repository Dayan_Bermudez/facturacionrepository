package sitiapp.evaluacion.security.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import sitiapp.evaluacion.model.Perfil;
import sitiapp.evaluacion.model.Respuesta;
import sitiapp.evaluacion.model.Usuario;
import sitiapp.evaluacion.security.dto.JwtDto;
import sitiapp.evaluacion.security.dto.LoginUsuario;
import sitiapp.evaluacion.security.dto.NuevoUsuario;
import sitiapp.evaluacion.security.jwt.JwtProvider;
import sitiapp.evaluacion.security.service.PerfilSecurityService;
import sitiapp.evaluacion.service.UsuarioService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;

@RestController
@RequestMapping("/auth")
@CrossOrigin
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtProvider jwtProvider;

    @PostMapping("/login")
    public ResponseEntity<Respuesta> login(@Valid @RequestBody LoginUsuario loginUsuario, BindingResult bindingResult){
        Respuesta respuesta = new Respuesta();
        if(bindingResult.hasErrors()){
            respuesta.mensaje = "campos mal puestos";
            respuesta.exito = 0;
            return ResponseEntity.ok(respuesta);
        }

        try{
            Authentication authentication = authenticationManager.authenticate(new
                    UsernamePasswordAuthenticationToken(loginUsuario.getUsuario(),loginUsuario.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtProvider.generateToken(authentication);
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            Collection<? extends GrantedAuthority> grantedAuthorities = userDetails.getAuthorities();
            Object[] perfiles = grantedAuthorities.toArray();
            String perfil = perfiles[0].toString();
            JwtDto jwdto = new JwtDto(jwt,userDetails.getUsername(),perfil);
            respuesta.data = jwdto;
            respuesta.exito = 1;
        }catch (Exception exception){
            respuesta.exito = 0;
            respuesta.mensaje = exception.getMessage();
        }
        return ResponseEntity.ok(respuesta);
    }
}
