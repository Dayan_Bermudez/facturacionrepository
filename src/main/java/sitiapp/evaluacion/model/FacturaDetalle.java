package sitiapp.evaluacion.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "factura_detalle")
@Data
@Entity
public class FacturaDetalle implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "factura_id")
    private Factura factura;

    @ManyToOne
    @JoinColumn(name = "producto_id")
    private Producto producto;

    @Column(name = "cantidad")
    private Integer cantidad;

    @Column(name = "valor_unitario")
    private Integer valorUnitario;

    @Column(name = "total")
    private Integer total;

    @Column(name = "activo")
    private Integer activo;

    public FacturaDetalle() {
    }

    public FacturaDetalle(Integer id, Factura factura, Producto producto, Integer cantidad, Integer valorUnitario, Integer total, Integer activo) {
        this.id = id;
        this.factura = factura;
        this.producto = producto;
        this.cantidad = cantidad;
        this.valorUnitario = valorUnitario;
        this.total = total;
        this.activo = activo;
    }
}