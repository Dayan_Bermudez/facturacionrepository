package sitiapp.evaluacion.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "tipos_identificacion")
@Data
@Entity
public class TiposIdentificacion implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipo", nullable = false)
    private Integer id;

    @Column(name = "abreviatura", length = 3)
    private String abreviatura;

    @Column(name = "descripcion", length = 100)
    private String descripcion;

    public TiposIdentificacion() {
    }

    public TiposIdentificacion(Integer id, String abreviatura, String descripcion) {
        this.id = id;
        this.abreviatura = abreviatura;
        this.descripcion = descripcion;
    }
}