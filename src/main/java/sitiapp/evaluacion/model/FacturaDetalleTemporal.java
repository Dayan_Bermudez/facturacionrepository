package sitiapp.evaluacion.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "factura_detalle_temporal")
@Data
@Entity
public class FacturaDetalleTemporal implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "producto_id")
    private Producto producto;

    @Column(name = "cantidad")
    private Integer cantidad;

    @Column(name = "valor_unitario")
    private Integer valorUnitario;

    @Column(name = "total")
    private Integer total;

    public FacturaDetalleTemporal() {
    }

    public FacturaDetalleTemporal(Integer id, Producto producto, Integer cantidad, Integer valorUnitario, Integer total) {
        this.id = id;
        this.producto = producto;
        this.cantidad = cantidad;
        this.valorUnitario = valorUnitario;
        this.total = total;
    }
}