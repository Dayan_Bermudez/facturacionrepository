package sitiapp.evaluacion.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "producto")
@Data
@Entity
public class Producto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_producto", nullable = false)
    private Integer id;

    @Column(name = "nombre", length = 100)
    private String nombre;

    @Column(name = "estado", length = 45)
    private String estado;

    @Column(name = "valor_unitario")
    private Integer valorUnitario;

    @Column(name = "activo")
    private Integer activo;

    public Producto() {
    }

    public Producto(Integer id, String nombre, String estado, Integer valorUnitario, Integer activo) {
        this.id = id;
        this.nombre = nombre;
        this.estado = estado;
        this.valorUnitario = valorUnitario;
        this.activo = activo;
    }
}