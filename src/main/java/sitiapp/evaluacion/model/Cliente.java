package sitiapp.evaluacion.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Table(name = "cliente")
@Entity
public class Cliente implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cliente", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_tipo_identificacion")
    private TiposIdentificacion idTipoIdentificacion;

    @Column(name = "identificacion", length = 100)
    private String identificacion;

    @Column(name = "razon_social", length = 100)
    private String razonSocial;

    @Column(name = "fecha_registro")
    private LocalDate fechaRegistro;

    @Column(name = "estado", length = 1)
    private String estado;

    @Column(name = "activo")
    private Integer activo;

    public Cliente() {
    }

    public Cliente(Integer id, TiposIdentificacion idTipoIdentificacion, String identificacion, String razonSocial, LocalDate fechaRegistro, String estado, Integer activo) {
        this.id = id;
        this.idTipoIdentificacion = idTipoIdentificacion;
        this.identificacion = identificacion;
        this.razonSocial = razonSocial;
        this.fechaRegistro = fechaRegistro;
        this.estado = estado;
        this.activo = activo;
    }
}