package sitiapp.evaluacion.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Table(name = "factura")
@Data
@Entity
public class Factura implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura", nullable = false)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;

    @Column(name = "fecha")
    private LocalDate fecha;

    @Column(name = "total_factura")
    private Integer totalFactura;

    @Column(name = "activo")
    private Integer activo;

    public Factura() {
    }

    public Factura(Integer id, Cliente cliente, LocalDate fecha, Integer totalFactura, Integer activo) {
        this.id = id;
        this.cliente = cliente;
        this.fecha = fecha;
        this.totalFactura = totalFactura;
        this.activo = activo;
    }
}