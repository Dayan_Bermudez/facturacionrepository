package sitiapp.evaluacion.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "usuario")
@Data
@Entity
public class Usuario implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario", nullable = false)
    private Integer id;

    @Column(name = "nombre", length = 45)
    private String nombre;

    @Column(name = "apellido", length = 45)
    private String apellido;

    @Column(name = "usuario", length = 45)
    private String usuario;

    @Column(name = "\"contraseña\"", length = 45)
    private String contraseña;

    @ManyToOne
    @JoinColumn(name = "perfil_id")
    private Perfil perfil;

    @Column(name = "activo")
    private Integer activo;

    public Usuario() {
    }

    public Usuario(Integer id, String nombre, String apellido, String usuario, String contraseña, Perfil perfil, Integer activo) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.perfil = perfil;
        this.activo = activo;
    }
}