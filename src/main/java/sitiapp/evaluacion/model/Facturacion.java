package sitiapp.evaluacion.model;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Facturacion implements Serializable {
    private Factura factura;
    private List<FacturaDetalle> facturaDetalle;
    private List<FacturaDetalleTemporal> facturaDetalleTemporal;

    public Facturacion() {
    }

    public Facturacion(Factura factura, List<FacturaDetalle> facturaDetalle ,List<FacturaDetalleTemporal> facturaDetalleTemporal) {
        this.factura = factura;
        this.facturaDetalle = facturaDetalle;
        this.facturaDetalleTemporal = facturaDetalleTemporal;
    }
}
