package sitiapp.evaluacion.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
public class FacturacionGetBase implements Serializable {
    private Integer id_factura;
    private LocalDate fecha;
    private Integer total_factura;
    private Integer id_producto;
    private String nombre;
    private Integer cantidad;
    private Integer valor_unitario;
    private Integer total;
    private String abreviatura;
    private String identificacion;
    private String razon_social;

    public FacturacionGetBase() {
    }

    public FacturacionGetBase(Integer id, LocalDate fecha, Integer totalFactura, Integer productoId, String nombre, Integer cantidad, Integer valorUnitario, Integer total, String abreviatura, String identificacion, String razonSocial) {
        this.id_factura = id;
        this.fecha = fecha;
        this.total_factura = totalFactura;
        this.id_producto = productoId;
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.valor_unitario = valorUnitario;
        this.total = total;
        this.abreviatura = abreviatura;
        this.identificacion = identificacion;
        this.razon_social = razonSocial;
    }
}
