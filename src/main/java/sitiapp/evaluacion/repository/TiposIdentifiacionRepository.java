package sitiapp.evaluacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sitiapp.evaluacion.model.TiposIdentificacion;

import java.util.List;

@Repository
public interface TiposIdentifiacionRepository extends JpaRepository<TiposIdentificacion,Integer> {

    @Query("select new TiposIdentificacion (tpi.id,tpi.abreviatura,tpi.descripcion) from TiposIdentificacion tpi " +
            "order by tpi.id")
    List<TiposIdentificacion> findAllOrderById();
}
