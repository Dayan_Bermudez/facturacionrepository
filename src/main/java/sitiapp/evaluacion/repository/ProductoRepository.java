package sitiapp.evaluacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sitiapp.evaluacion.model.Producto;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProductoRepository extends JpaRepository<Producto,Integer> {

    @Override
    @Query("select new Producto (p.id,p.nombre,p.estado,p.valorUnitario,p.activo) " +
            "from Producto p where p.activo <> 0 order by p.id")
    List<Producto> findAll();

    @Query("select new Producto (p.id,p.nombre,p.estado,p.valorUnitario,p.activo) " +
            "from Producto p where p.activo <> 0 and p.id=:id_producto")
    Optional<Producto> findById(@Param("id_producto") Integer integer);

    @Transactional
    @Modifying
    @Query("UPDATE Producto p SET p.id=:id_producto,p.nombre=:nombre,p.estado=:estado,p.valorUnitario=:valor_unitario WHERE p.id=:id_producto")
    Integer updateProducto(@Param("id_producto") Integer id,@Param("nombre") String nombre,@Param("estado") String estado,@Param("valor_unitario") Integer valorUnitario);

    @Transactional
    @Modifying
    @Query("UPDATE Producto p SET p.activo=0 where p.id=:id_producto")
    Integer deleteProducto(@Param("id_producto") Integer id_producto);

    @Override
    boolean existsById(Integer integer);
    
    boolean existsByNombre(String nombre);
}
