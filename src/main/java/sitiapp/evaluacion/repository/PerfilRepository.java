package sitiapp.evaluacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sitiapp.evaluacion.model.Perfil;

import java.util.List;
import java.util.Optional;

@Repository
public interface PerfilRepository extends JpaRepository<Perfil,Integer> {

    @Override
    @Query("select new Perfil (p.id,p.nombre) " +
            "from Perfil p order by p.id")
    List<Perfil> findAll();

    @Query("select new Perfil (p.id,p.nombre) from Perfil p where p.nombre like %:nombre%")
    Optional<Perfil> findByNombre(@Param("nombre") String nombre);
}
