package sitiapp.evaluacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sitiapp.evaluacion.model.FacturaDetalleTemporal;

@Repository
public interface FacturaDetalleTemporalRepository extends JpaRepository<FacturaDetalleTemporal,Integer> {
}
