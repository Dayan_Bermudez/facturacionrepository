package sitiapp.evaluacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sitiapp.evaluacion.model.Cliente;
import sitiapp.evaluacion.model.FacturaDetalle;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface FacturaDetalleRepository extends JpaRepository<FacturaDetalle,Integer> {

    @Query("select new FacturaDetalle (f.id,f.factura,f.producto,f.cantidad,f.valorUnitario,f.total,f.valorUnitario) " +
            "from FacturaDetalle f " +
            "left join Factura fac on fac.id = f.factura.id " +
            "left join Producto p on p.id = f.producto.id " +
            "where f.activo <> 0 " +
            "order by f.id")
    List<FacturaDetalle> findAll();

    @Query("select new FacturaDetalle (f.id,f.factura,f.producto,f.cantidad,f.valorUnitario,f.total,f.valorUnitario) " +
            "from FacturaDetalle f " +
            "left join Factura fac on fac.id = f.factura.id " +
            "left join Producto p on p.id = f.producto.id " +
            "where f.activo <> 0 and f.factura.id=:id_factura " +
            "order by f.id")
    List<FacturaDetalle> findAllByFacturaId(@Param("id_factura") Integer id_factura);

    @Transactional
    @Modifying
    @Query("UPDATE FacturaDetalle f SET f.activo=0 where f.factura.id=:id_factura")
    Integer deleteFacturaDetalleByFacturaId(@Param("id_factura") Integer id_factura);
}
