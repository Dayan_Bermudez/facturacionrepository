package sitiapp.evaluacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sitiapp.evaluacion.model.Perfil;
import sitiapp.evaluacion.model.Usuario;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario,Integer> {

    @Query("select new Usuario (us.id,us.nombre,us.apellido,us.usuario,us.contraseña,us.perfil,us.activo) " +
            "from Usuario us left join Perfil pf on pf.id= us.perfil.id " +
            "where us.activo <> 0 " +
            "order by us.id")
    List<Usuario> findAll();

    Optional<Usuario> findByUsuario(String usuario);

    boolean existsByUsuario(String usuario);

    @Query("select new Usuario (us.id,us.nombre,us.apellido,us.usuario,us.contraseña,us.perfil,us.activo) " +
            "from Usuario us left join Perfil pf on pf.id= us.perfil.id " +
            "where us.activo <> 0 and us.id=:id_usuario " +
            "order by us.id")
    Optional<Usuario> findById(@Param("id_usuario") Integer id_usuario);

    @Transactional
    @Modifying
    @Query("UPDATE Usuario us SET us.id=:id_usuario,us.nombre=:nombre,us.apellido=:apellido,us.usuario=:usuario,us.perfil=:perfil,us.contraseña=:contraseña WHERE us.id=:id_usuario")
    Integer updateUsuario(@Param("id_usuario") Integer id,@Param("nombre") String nombre,@Param("apellido") String apellido,@Param("usuario") String usuario,@Param("contraseña") String contraseña,@Param("perfil") Perfil perfil);

    @Transactional
    @Modifying
    @Query("UPDATE Usuario us SET us.activo=0 where us.id=:id_usuario")
    Integer deleteUsuario(@Param("id_usuario") Integer id_usuario);
}
