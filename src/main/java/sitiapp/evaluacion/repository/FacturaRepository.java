package sitiapp.evaluacion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sitiapp.evaluacion.model.Factura;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface FacturaRepository extends JpaRepository<Factura,Integer> {

    @Query("select new Factura(f.id,f.cliente,f.fecha,f.totalFactura,f.activo) " +
            "from Factura f left join Cliente c on c.id = f.id " +
            "where f.activo <> 0 " +
            "order by f.id")
    List<Factura> findAll();

    @Query("select new Factura(f.id,f.cliente,f.fecha,f.totalFactura,f.activo) " +
            "from Factura f left join Cliente c on c.id = f.id " +
            "where f.activo <> 0 and f.id=:id_factura")
    Optional<Factura> findById(@Param("id_factura") Integer id_factura);

    @Transactional
    @Modifying
    @Query("UPDATE Factura f SET f.activo=0 where f.id=:id_factura")
    Integer deleteFactura(@Param("id_factura") Integer id_factura);

    @Query("select max(f.id) from Factura f")
    Integer getTopBy();
}
