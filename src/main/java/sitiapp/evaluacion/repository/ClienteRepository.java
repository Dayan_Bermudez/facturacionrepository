package sitiapp.evaluacion.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sitiapp.evaluacion.model.Cliente;
import sitiapp.evaluacion.model.TiposIdentificacion;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente,Integer> {

    @Query("select new Cliente(c.id,c.idTipoIdentificacion,c.identificacion,c.razonSocial,c.fechaRegistro, " +
            "c.estado,c.activo) " +
            "from Cliente c left join TiposIdentificacion ti on ti.id = c.idTipoIdentificacion.id " +
            "where c.activo <> 0 " +
            "order by c.id")
    List<Cliente> findAll();

    @Query("select new Cliente(c.id,c.idTipoIdentificacion,c.identificacion,c.razonSocial,c.fechaRegistro, " +
            "c.estado,c.activo) " +
            "from Cliente c left join TiposIdentificacion ti on ti.id = c.idTipoIdentificacion.id " +
            "where c.activo <> 0 and c.id=:id_cliente")
    Optional<Cliente> findById(@Param("id_cliente") Integer integer);

    @Transactional
    @Modifying
    @Query("UPDATE Cliente c SET c.id=:id,c.identificacion=:identificacion,c.razonSocial=:razon_social,c.fechaRegistro=:fecha_registro,c.estado=:estado,c.idTipoIdentificacion=:tipo_identificacion WHERE c.id=:id")
    Integer updateCliente(@Param("id") int id, @Param("identificacion")  String identificacion, @Param("razon_social") String razon_social, @Param("fecha_registro") LocalDate fecha_registro, @Param("estado") String estado, @Param("tipo_identificacion") TiposIdentificacion tipo_identificacion);

    @Transactional
    @Modifying
    @Query("UPDATE Cliente c SET c.activo=0 where c.id=:id_cliente")
    Integer deleteCliente(@Param("id_cliente") Integer id_cliente);

    @Query("SELECT new Cliente(c.id,c.idTipoIdentificacion,c.identificacion,c.razonSocial," +
            "c.fechaRegistro,c.estado,c.activo) FROM Cliente c " +
            "LEFT JOIN TiposIdentificacion ti on ti.id=c.idTipoIdentificacion.id" +
            " WHERE c.idTipoIdentificacion.id=:id_tipo_buscado AND c.identificacion LIKE %:identificacion% ")
    List<Cliente> getClientesByTipo_E_Idetificacion(@Param("id_tipo_buscado") Integer id_tipo_buscado, @Param("identificacion") String identificacion);

    @Query("SELECT new Cliente(c.id,c.idTipoIdentificacion,c.identificacion,c.razonSocial," +
            "c.fechaRegistro,c.estado,c.activo) FROM Cliente c " +
            "LEFT JOIN TiposIdentificacion ti on ti.id=c.idTipoIdentificacion.id" +
            " WHERE c.idTipoIdentificacion.id=:id_tipo_buscado")
    List<Cliente> getClientesByTipoIdetificacion(@Param("id_tipo_buscado") Integer id_tipo_buscado);

    @Query("SELECT new Cliente(c.id,c.idTipoIdentificacion,c.identificacion,c.razonSocial," +
            "c.fechaRegistro,c.estado,c.activo) FROM Cliente c " +
            "LEFT JOIN TiposIdentificacion ti on ti.id=c.idTipoIdentificacion.id" +
            " WHERE c.identificacion LIKE %:identificacion%")
    List<Cliente> getClientesByIdetificacion(@Param("identificacion") String identificacion);

    boolean existsByIdentificacion(String identificacion);
}
